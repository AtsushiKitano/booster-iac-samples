provider "google" {
  version = "2.8.0"
  project = "${terraform.workspace}"
  region = "${var.region}"
}

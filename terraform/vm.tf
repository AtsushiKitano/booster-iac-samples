resource "google_compute_instance" "server" {
  name         = "server"
  machine_type = "${local.instance_type["${terraform.workspace}"]}"
  zone         = "asia-northeast1-b"
  tags         = ["sv"]
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "${data.google_compute_image.centos_image.self_link}"
    }
  }

  network_interface {
    subnetwork = "${google_compute_subnetwork.server-vpc-subnet.self_link}"

    access_config {
      nat_ip = "${google_compute_address.server_static_ex_ip.address}"
    }
  }

  service_account {
    scopes = [
      "sql-admin",
      "storage-rw"
    ]
    email = "${google_service_account.server_service_account.email}"
  }
}

resource "google_compute_address" "server_static_ex_ip" {
  name         = "global-ip-server"
  address_type = "EXTERNAL"
}

resource "google_compute_network" "server-vpc" {
  name                    = "${var.service-name}-vpc"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "server-vpc-subnet" {
  name          = "${var.service-name}-vpc-subnet"
  ip_cidr_range = "${var.address-range}"
  network       = "${google_compute_network.server-vpc.self_link}"
}

data "google_compute_image" "centos_image" {
  family  = "centos-7"
  project = "centos-cloud"
}

resource "google_compute_firewall" "server-fw" {
  name    = "${var.service-name}-fw"
  network = "${google_compute_network.server-vpc.self_link}"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = [
      "22",
      "80",
      "443"
    ]
  }

  source_tags = ["sv"]
  source_ranges = [
    "0.0.0.0/0"
  ]
}

resource "google_service_account" "server_service_account" {
  account_id = "${var.service-name}-sa"
  display_name = "service account"
}

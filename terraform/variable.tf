variable "region" {
  default = "asia-northeast1"
}

variable "service-name" {
  default = "booster-demo"
}

variable "address-range" {
  default = "192.168.10.0/24"
}

locals {
  instance_type = {
    "project-dev" = "f1-micro"
    "project-prd" = "n1-standard-1"
  }
}

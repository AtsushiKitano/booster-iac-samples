GCPでのIaCサンプル
---

## 実行方法

### terraform

- workspaceの作成
```
$ terraform workspace new <workspace name>
```
- プロジェクト名を入力(variable.tfのproject-dev,project-devにproject-idを入力)
- terraformの初期化
```
$ terraform init
```
- terraform plan/apply
```
$ terraform apply
```

### docker

- Dockerfile-httpd-not-recommended: Dockerfileの非推奨の書き方(apacheのインストール)
- Dockerfile-httpd-bestpractice: Dockerfileのベストプラクティスの書き方(apacheのインストール)
- Dockerfile-date-command: dateコマンドを実行するDockerfile

#### 実行方法

- build
```
$ docker build -t <image name> -f <Dockerfile name> .
```

* run
    * Dockerfile-httpd-not-recommended
    ```
    $ docker run --privileged -itd --rm --stop-signal SIGRTMIN+3 --name <container name> -p <listen port>:80 <image name> /sbin/init
    ```
    * Dockerfile-httpd-not-recommended
    ```
    $ docker run -itd --rm -p <listen port>:80 --name <container name> <image name>
    ```
    * Dockerfile-date-command
    ```
    $ docker run --rm <image name>
    $ docker run --rm <image name> %Y
    ```
### kubernetes

- kubernetesクラスタに接続
```
$ gcloud container clusters get-credentials <clustername> --zone <cluster zone> --project <cluster project id>
```
- マニフェストファイルをapply
```
$ kubectl apply -f <manifest file>
```

#### 確認方法

- 実行コマンド
```
$ curl  -I http://<ingress IP>/test/
```
- 出力: Server: Apacheが出力される
```
HTTP/1.1 404 Not Found
Date: Thu, 03 Oct 2019 06:47:10 GMT
Server: Apache/2.4.41 (Unix)
Content-Type: text/html; charset=iso-8859-1
Via: 1.1 google
Transfer-Encoding: chunked
```

